# Copyleft

A serious game to learn free software licenses based on [2048 by Gabriele Cirulli](http://gabrielecirulli.github.io/2048).

![Screenshot](https://gitlab.com/fgallaire/copyleft/raw/master/img/screenshot.png)

Made just for fun. [Play it here!](https://f.gallai.re/copyleft)

## License
Copyleft is licensed under the [MIT license](https://gitlab.com/fgallaire/copyleft/raw/master/LICENSE.txt).
